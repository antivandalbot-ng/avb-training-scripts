import csv
import sys
import json
from pprint import pprint
import msgpack
from mwapi import Session
from revscoring.extractors import api
from revscoring.features import wikitext
from revscoring import errors
import simplewiki

with open("config.json") as c:
    config = json.load(c)
session = Session(config['wikiurl'], user_agent="User:{} ({})".format(config['wikiuser'], config['wiki']))
api_extractor = api.Extractor(session)

revs = []
rev_labels = []
values = {}
skip = []

try:
    with open("train-values.json") as f:
        combined = json.load(f)
except FileNotFoundError:
    combined = {}

try:
    with open("train-tokens.json") as f:
        tokens = json.load(f)
except FileNotFoundError:
    tokens = {}

with open("rev-main_classifier.txt", newline='') as revfile:
    revreader = csv.reader(revfile, delimiter=" ")
    for row in revreader:
        revs.append(row[0])
        rev_labels.append(row[1])

try:
    if sys.argv[1]:
        revs = revs[:int(sys.argv[1])]
except IndexError:
    pass
simplewiki.damaging = simplewiki.damaging[:-3]
simplewiki.damaging.append(wikitext.revision.diff.datasources.words_added)
try:
    for rev in revs:
        if combined.get(rev) is None:
            try:
                print("Retrieving {}...".format(rev))
                values[rev] = list(api_extractor.extract(int(rev), simplewiki.damaging))
            except (errors.RevisionNotFound, errors.TextDeleted, errors.CommentDeleted, errors.UserDeleted):
                print("Revision {} not found, skipping...".format(rev))
                skip.append(rev)
except KeyboardInterrupt:
    print("Ctrl-C received, cleaning up...")
finally:
    for r, l in zip(revs, rev_labels):
        if r in skip:
            continue
        elif values.get(r) is None:
            continue
        else:
            combined[r] = {}
            combined[r]['values'] = values.get(r)[:-1]
            combined[r]['label'] = l
            tokens[r] = {}
            tokens[r]['tokens'] = values.get(r)[-1]
            tokens[r]['label'] = l

    with open("train-values.json", "w") as f:
        json.dump(combined, f)

    with open("train-tokens.json", "w") as f:
        json.dump(tokens, f)

    with open("train-values.msgpack", "wb") as f:
        msgpack.dump(combined, f, use_bin_type=True)

    with open("train-tokens.msgpack", "wb") as f:
        msgpack.dump(tokens, f, use_bin_type=True)
