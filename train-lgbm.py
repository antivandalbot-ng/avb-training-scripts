import json
import numpy as np
import lightgbm as lgb
import pickle
from nltk.corpus import stopwords
from tqdm import tqdm
import spacy

import custom_features
import lgbmclassifier
import simplewiki

with open("train-values.json") as f:
    combined = json.load(f)

with open("train-tokens.json") as f:
    token_set = json.load(f)

with open("config.json") as c:
    config = json.load(c)

nlp = spacy.load("en_core_web_sm", disable=["parser", "ner"])
Tfidf_vect = pickle.load(open(config['tfidfmodel'], "rb"))
nb = pickle.load(open(config['naivebayesmodel'], "rb"))

train_data = []
model = lgbmclassifier.LGB(simplewiki.damaging, labels=["good", "vandalism"], version="0.0.9", max_depth=-1,
                           random_state=1, metric='auc', first_metric_only=True)

for k, v in tqdm(combined.items()):
    words = token_set.get(str(k))['tokens']
    doc = nlp(' '.join(words))
    tokens = ["{}_{}".format(token.text.lower(), token.tag_) for token in doc if not token.is_stop]
    tfidf = Tfidf_vect.transform([' '.join(tokens)])
    v['values'].append(nb.predict_proba(tfidf)[0][1])
    v['values'].append(custom_features._process_female_names(words))
    v['values'].append(custom_features._process_male_names(words))
    train_data.append((v['values'], v['label']))
param = {'num_leaves': 40,
         'objective': 'binary',
         'bagging_fraction': 0.75,
         'bagging_freq': 3,
         'num_iterations': 250,
         'learning_rate': 0.02,
         'lambda_l2': 0.05,
         }
model.set_params(**param)
model.train(train_data)
with open("simplewiki.lgbm.model", "w") as f:
    model.dump(f)
