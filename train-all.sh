#!/bin/sh
./sort-all.sh
python get_values.py
python get_tokens.py
python generate_tokens.py
python train-naivebayes.py
python train-lgbm.py
read -p "Send SIGHUP to bot? [y/n] " yn
case $yn in
  [Yy]* ) supervisorctl signal HUP antivandalbot;;
esac
