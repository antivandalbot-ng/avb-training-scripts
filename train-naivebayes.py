import json
import pickle

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import naive_bayes
from nltk.corpus import stopwords
from tqdm import tqdm
import spacy

with open("tokens-combined.json") as f:
    combined = json.load(f)

nlp = spacy.load("en_core_web_sm", disable=["parser", "ner"])
corpus = {}
corpus['text'] = []
corpus['label'] = []
for entry in tqdm(combined):
    doc = nlp(' '.join(entry['text']))
    tokens = ["{}_{}".format(token.text.lower(), token.tag_) for token in doc if not token.is_stop]
    tokens = ' '.join(tokens)
    corpus['text'].append(tokens)
    corpus['label'].append(entry['label'])
Tfidf_vect = TfidfVectorizer(max_features=8000, ngram_range=(1,2), sublinear_tf=True, strip_accents='unicode', min_df=2)
Tfidf_vect.fit(corpus['text'])
Naive = naive_bayes.ComplementNB()
Naive.fit(Tfidf_vect.transform(corpus['text']), corpus['label'])

with open("tfidf.model", "wb") as fp:
    pickle.dump(Tfidf_vect, fp, protocol=4)
with open("naivebayes.model", "wb") as fp:
    pickle.dump(Naive, fp, protocol=4)