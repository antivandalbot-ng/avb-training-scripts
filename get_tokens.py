import csv
import json
from pprint import pprint
import sys
import time

from mwapi import Session
from revscoring.features import wikitext
from revscoring.extractors import api
from revscoring import errors

with open("config.json") as c:
    config = json.load(c)
session = Session(config['wikiurl'], user_agent="User:{} ({})".format(config['wikiuser'], config['wiki']))
api_extractor = api.Extractor(session)

revs = []
rev_labels = []
values = {}
skip = []

try:
    with open("tokens-raw.json") as f:
        raw = json.load(f)
except FileNotFoundError:
        raw = {}

with open("rev-nb_classifier.txt", newline='') as revfile:
    revreader = csv.reader(revfile, delimiter=" ")
    for row in revreader:
        revs.append(row[0])
        rev_labels.append(row[1])

try:
    if sys.argv[1]:
        revs = revs[:int(sys.argv[1])]
except IndexError:
    pass

features = [wikitext.revision.diff.datasources.words_added]

try:
    for rev in revs:
        if raw.get(rev) is None:
            try:
                print("Retrieving {}...".format(rev))
                values[rev] = list(api_extractor.extract(int(rev), features))
            except errors.RevisionNotFound:
                print("Revision {} not found, skipping...".format(rev))
                skip.append(rev)
except KeyboardInterrupt:
    print("Ctrl-C received, cleaning up...")
finally:
    for r, l in zip(revs, rev_labels):
        if r in skip:
            continue
        elif values.get(r) is None:
            continue
        else:
            raw[r] = {}
            raw[r]['tokens'] = values.get(r)[0]
            raw[r]['label'] = l

    with open("tokens-raw.json", "w") as f:
        json.dump(raw, f)
