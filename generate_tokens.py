import csv
import json
from pprint import pprint
import sys
import time

from nltk.corpus import stopwords
from tqdm import tqdm

revs = []
rev_labels = []
values = {}
skip = []

with open("tokens-raw.json") as f:
    raw1 = json.load(f)

with open("train-tokens.json") as f:
    raw2 = json.load(f)

raw = {**raw1, **raw2}
combined = []

for rev in raw.values():
    entry = {}
    #tokens = [word for word in rev['tokens'] if word not in stopwords.words('english')]
    #entry['text'] = ' '.join(tokens).lower()
    entry['text'] = rev['tokens']
    entry['label'] = rev['label']
    combined.append(entry)

with open("tokens-combined.json", "w") as f:
    json.dump(combined, f)
